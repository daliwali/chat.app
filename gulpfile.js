// node system dependencies
var path = require('path');


// require gulp & gulp plugins
var gulp = require('gulp');
var myth = require('gulp-myth');
var minify = require('gulp-minify-css');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var connect = require('gulp-connect');
var header = require('gulp-header');
var handlebars = require('gulp-handlebars');
var defineModule = require('gulp-define-module');


// JSON configuration files
var config = require('./src/config.json');
var pkg = require('./package.json');


// source code notice
var banner = ['/*', '',
  '  The full, unminified source code is available here:',
  '  ' + pkg.repository.url, '', '*/', ''].join('\n');


// define build tasks
var build = ['css', 'js', 'copy'];


// task definitions
// ================

gulp.task('css', function () {
  return gulp.src(config.src.css.main)
    .pipe(myth())
    .pipe(minify({
      keepSpecialComments: 0
    }))
    .pipe(header(banner))
    .pipe(gulp.dest(path.join(
      config.dest.root,
      config.dest.assets)));
});


gulp.task('js', ['templates'], function () {
  return gulp.src(config.src.js.main)
    .pipe(browserify())
    .pipe(uglify())
    .pipe(header(banner))
    .pipe(gulp.dest(path.join(
      config.dest.root,
      config.dest.assets)));
});


gulp.task('templates', function() {
  return gulp.src(config.src.templates)
    .pipe(handlebars())
    .pipe(defineModule('node'))
    .pipe(gulp.dest('src/js/templates/'));
});


gulp.task('copy', function () {
  return gulp.src(config.src.static)
    .pipe(gulp.dest(config.dest.root));
});


gulp.task('watch', function () {
  gulp.watch(config.src.css.dir, ['css']);
  gulp.watch(config.src.js.dir, ['js']);
  gulp.watch(config.src.templates, ['templates']);
  gulp.watch(config.src.static, ['copy']);
});


gulp.task('connect', build, function () {
  return connect.server({
    root: config.dest.root,
    port: process.env.PORT || 5000,
    middleware: function (connect, options) {
      var middlewares = [];

        middlewares.push(connect.compress());

      return middlewares;
    }
  });
});


// main tasks
// ==========

gulp.task('build', build);
gulp.task('serve', build.concat('connect'));
gulp.task('default', build.concat('connect', 'watch'));
