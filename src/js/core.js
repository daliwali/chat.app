// 3rd party libraries
var _ = require('lodash');


// local
var modules = {
	Store: require('./store'),
	DOMInterface: require('./dom_interface'),
	Router: require('./router')
};


/**
 * Core is a namespace that contains everything that we want to do.
 */
var Core = {

	init: function () {
		var _this = this;

		// instantiate modules
		_.forEach(modules, function (mod, key) {
			_this[key] = new mod();
		});

		// inject references
		_.forEach(_.keys(modules), function (mod) {
			mod = _this[mod];
			_.forEach(mod.needs, function (req) {
				mod.ref[req] = _this[req];
			});
		});
	},

	destroy: function () {
		// TODO: teardown method
	}

};


module.exports = Core;
