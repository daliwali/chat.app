// load ES6 Promise shim.
window.Promise = window.Promise || require('es6-promise');


// require entry point for the app.
var App = require('./core');


// initialize the app, which will give us all the other things we will need to call.
App.init();


// fetch the data, which will then be rendered.
App.Store.fetch('assets/sample-chat-data.json');
