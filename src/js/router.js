/**
 * A simple router implementation using location hashes.
 */
function Router () {
	this._init();
}


Router.prototype.needs = ['DOMInterface'];


Router.prototype._init = function () {
	var _this = this;

	this.ref = {};

	window.onhashchange = function (event) {
		var hash = getHash();

		if (!!hash) {
			_this.ref.DOMInterface.trigger('goto', hash);
		} else {
			_this.ref.DOMInterface.trigger('goto', 'home');
		}
	};
};


/**
 * Transition to an ID.
 *
 * @param {String} id
 */
Router.prototype.transitionTo = function (id) {
	var location = window.location;

	if (!!id) {
		location.hash = id;
	} else {
		location.hash = '';
	}
};


Router.prototype.getLocation = function () {
	return getHash();
};


module.exports = Router;


function getHash () {
	return (window.location.hash || '').substr(1);
}
