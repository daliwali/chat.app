// node system libraries
var URL = require('url');
var path = require('path');


// require 3rd party libraries
var Oboe = require('oboe');
var _ = require('lodash');


/**
 * The purpose of this module is to provide facilities for fetching
 * data from remote servers, and storing data in a useful way.
 */
function Store () {
	this._init();
}


Store.prototype.needs = ['DOMInterface', 'Router'];


Store.prototype._init = function () {
	this._data = [];
	this.meta = {
		count: 0
	};
	this.ref = {};
};


/**
 * Use the AJAX feature provided by Oboe to fetch data from a
 * remote server.
 *
 * @param {String} url the URL to fetch
 */
Store.prototype.fetch = function (url) {
	var _this = this;
	var location = window.location;

	// check if we should assume relative path or not.
	if (!url.match(/^(http|https):\/\//g)) {
		url = URL.format({
			protocol: location.protocol,
			host: location.host,
			pathname: path.join(location.pathname, url)
		});
	}

	var stream = Oboe(url);

	this.processStream(stream);

	return stream;
};


/**
 * Get an object from the store.
 *
 * @param {String} id
 */
Store.prototype.get = function (id) {
	return _.find(this._data, function (obj) {
		return obj.id === id;
	});
};


/**
 * Interpret the data that we receive from the server.
 *
 * @param {Object} stream
 */
Store.prototype.processStream = function (stream) {
	var _this = this;

	stream.node('!.*', function (node, path) {
		_this.ref.DOMInterface.toggleSidebar(true);

		if (!_this.ref.DOMInterface._cache.main.innerHTML.trim().length) {
			_this.ref.DOMInterface.render('main', {});
		}

		_this._data.push(node);
		_this.meta.count++;

		// sort by reverse chronological order
		_this._data = _.sortBy(_this._data, function (obj) {
			return obj.created_at;
		}).reverse();

		_this.ref.DOMInterface.render('sidebar', _this._data);
		_this.ref.DOMInterface.render('header', _this.meta);

		// if this id matches the current location, render it
		if (node.id === _this.ref.Router.getLocation()) {
			// need to wait for next tick because of handlebars rendering
			setTimeout(function () {
				_this.ref.DOMInterface.trigger('goto', node.id);
			}, 0);
		}
	});

}


module.exports = Store;
