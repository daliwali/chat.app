// require templates
var _ = require('lodash');
var Handlebars = require('handlebars');
var templates = {};


Handlebars.registerHelper('timestamp', require('../helpers/timestamp'));
Handlebars.registerHelper('cut_off', require('../helpers/cut_off'));


templates.sidebar = require('./templates/sidebar');
templates.header = require('./templates/header');
templates.main = require('./templates/main');


/**
 * This module is a collection of methods to manipulate the DOM.
 * The specific implementation can be changed to use whatever
 * DOM manipulation libraries such as jQuery/Zepto/React/Ractive,
 * but for now it's vanilla JS.
 */
function DOM () {
	this.ref = {};
	this.cacheSelectors();
	this.addEventListeners();
}


DOM.prototype.needs = ['Router', 'Store'];


/**
 * Cache selectors for use in other methods.
 */
DOM.prototype.cacheSelectors = function () {
	var cache = this._cache = {};
	var qs = document.querySelector;
	var qsa = document.querySelectorAll;

	cache.sidebarContainer = qs.call(document, 'main > aside');
	cache.sidebar = qs.call(document, '.sidebar-sink');
	cache.header = qs.call(document, '.header-sink');
	cache.main = qs.call(document, '.main-sink');
};


/**
 * Add event listeners to the document.
 */
DOM.prototype.addEventListeners = function () {
	var _this = this;

	// sidebar menu
	document.addEventListener('click', function (event) {
		var target = event.target;
		var itemClass = 'sidebar-item';
		var data;

		if (~target.className.indexOf(itemClass)) {
			_.forEach(document.querySelectorAll('.' + itemClass), function (item) {
				item.className = itemClass;
			});
			target.className = itemClass + ' active';

			_this.ref.Router.transitionTo(target.dataset.id);

			data = _this.ref.Store.get(target.dataset.id);
			_this.render('header', data);
			_this.render('main', data);
		}

		if (~target.className.indexOf('header-logo')) {
			_.forEach(document.querySelectorAll('.' + itemClass), function (item) {
				item.className = itemClass;
			});

			_this.ref.Router.transitionTo(null);

			_this.render('header', _this.ref.Store.meta);
			_this.render('main', {});
		}
	}, false);

};


/**
 * Toggles the sidebar visibility.
 *
 * @param {Boolean} [toggle] manually override state
 */
DOM.prototype.toggleSidebar = function (toggle) {
	var sidebar = this._cache.sidebarContainer;

	if (!toggle && !sidebar.className) {
		sidebar.className = 'hidden';
	} else if (!!toggle) {
		sidebar.className = '';
	}
};


/**
 * Render the template with provided template data.
 *
 * @param {String} template
 * @param {Object} data
 */
DOM.prototype.render = function (template, data) {
	this._cache[template].innerHTML = templates[template](data);
};


/**
 * Handle programmatic events.
 *
 * @param {String} type
 * @param {String} id
 */
DOM.prototype.trigger = function (type, id) {
	if (type === 'goto') {
		if (id !== 'home') {
			document.querySelector('[data-id="' + id + '"]').click();
		} else {
			document.querySelector('.header-logo').click();
		}
	}
};


module.exports = DOM;
