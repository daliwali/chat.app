module.exports = function (number) {
	number = parseInt(number, 10) * 1000;

	return new Date(number).toLocaleString();
};
