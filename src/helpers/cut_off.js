module.exports = function (str, len) {
	if (typeof len === 'number' && str.length > len) {
		return str.substr(0, len) + '...';
	} else {
		return str;
	}
};
