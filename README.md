# Chat.app

A demo made for SnapEngage.


## Build Instructions

You will need Node.js installed on your system. Then to build and run a local server:

```sh
$ npm install && npm start
```

A server should be running at `localhost:5000`.


## Explanation

I wanted to demonstrate what I already know, combined with some new technologies I have not worked with before.

Gulp is used for the build process, which manages compilation of static assets such as CSS, JS, and templates while developing. I opted to use Browserify for module management, because it makes using Node.js standard libraries in the front-end easy, and CommonJS is already a well-established standard.

Upon loading the page, one of the first things you may notice is the loading and initial states. The sidebar is shown immediately after the first object in the payload is parsed, and the objects are retroactively sorted in reverse chronological order. This is possible using [Oboe](http://oboejs.com), which allows for partially loaded JSON to be rendered. The effect is exaggerated when a user downloads a large payload or is on a slow connection. The code lives in the Store which caches loaded objects and fetches things. The most interesting part about the Store is that the DOMInterface is notified when objects are ready from a partially loaded JSON stream.

The state of the page is managed by the Router, which handles state changes and will initialize the app on the view corresponding to the URL. The implementation uses the simple `window.location.hash`, though the History API would be a better option, but I don't want to worry about configuring a server to respond to arbitrary URLs for this demo. The Router talks to the DOMInterface when the URL changes.

In terms of design, I looked to common features of email clients and chat applications, and aimed for a semi-flat aesthetic.


### Technologies

- HTML5 / ES5 / CSS3
- Gulp
- Oboe
- Handlebars
- Lodash


### Compatibility

This is designed to work with IE10 and above, due to the use of ES5 features and some CSS3, and I don't have time to include shims and workarounds for the purposes of this demo.
